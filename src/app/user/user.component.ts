import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['userVar']
})
export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  @Output() editEvent = new EventEmitter<User>();
  userVar:User;//היוזר ממסד הנתונים
  isEdit: Boolean = false;
  editButtonText = "Edit";
  tempUser:User = {name: null, email:null};

  sendDelete(){
    this.deleteEvent.emit(this.userVar);
  }
  toggleEdit(){
    this.isEdit =! this.isEdit;
    this.isEdit? this.editButtonText = "Save": this.editButtonText = "Edit";
    if (this.isEdit){
      this.tempUser.name=this.userVar.name;
      this.tempUser.email = this.userVar.email;
    }
    if (!this.isEdit){
      this.editEvent.emit(this.userVar);
    }
  }
  sendCancle(){
    this.userVar.name = this.tempUser.name;
    this.userVar.email = this.tempUser.email;
  }
  constructor() { }

  ngOnInit() {
  }

}
