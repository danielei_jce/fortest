import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class UsersService {
//private _url = 'https://jsonplaceholder.typicode.com/users';
usersobservable;
getUsers() {
    this.usersobservable = this.af.database.list('/users'); 
     return this.usersobservable;
   }
   addUser(userF){
     this.usersobservable.push(userF);
   }
   updateUser(user){
     let userKey = user.$key;
     let userData = {name:user.name, email:user.email}
     this.af.database.object('/users/'+ userKey).update(userData);
   }
   deleteUser(user){
     let userKey = user.$key;
     this.af.database.object('/users/'+userKey).remove();
   
   }

  constructor(private af:AngularFire) { }

}
