import { FortestPage } from './app.po';

describe('fortest App', function() {
  let page: FortestPage;

  beforeEach(() => {
    page = new FortestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
